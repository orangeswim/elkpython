# elkpython

1) Install elasticsearch6 python module
```bash
chmod +x python-install.sh
./python-install.sh
```
2) User docker-compose to start the stack
```bash
docker-compose up -d
```

3) Run the example.py based off elasticsearch-py documentation
```bash
python example.py
```

Result
```bash
$ python example.py
created
{u'text': u'Elasticsearch: cool. bonsai cool.', u'author': u'kimchy', u'timestamp': u'2019-07-16T20:34:33.426831'}
Got 1 Hits:
2019-07-16T20:34:33.426831 kimchy: Elasticsearch: cool. bonsai cool.
```